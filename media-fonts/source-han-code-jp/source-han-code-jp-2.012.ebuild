# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit font

DESCRIPTION="Source Han Code JP font family"
HOMEPAGE="https://github.com/adobe-fonts/source-han-code-jp"
SRC_URI="https://github.com/adobe-fonts/source-han-code-jp/releases/download/${PV}R/SourceHanCodeJP.ttc -> ${P}.ttc"
S="${WORKDIR}"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="binchecks mirror strip"

FONT_SUFFIX="ttc"

src_unpack() {
	cp "${DISTDIR}/${P}.ttc" "${S}"/SourceHanCodeJP.ttc || die
}
