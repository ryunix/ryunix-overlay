# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit font

DESCRIPTION="Source Han Sans JP font family"
HOMEPAGE="https://github.com/adobe-fonts/source-han-sans"
SRC_URI="https://github.com/adobe-fonts/source-han-sans/releases/download/${PV}R/SourceHanSansJP.zip -> ${P}.zip"
S="${WORKDIR}"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="binchecks mirror strip"

BDEPEND="app-arch/unzip"

FONT_SUFFIX="otf"
FONT_S="${S}/SubsetOTF/JP"
