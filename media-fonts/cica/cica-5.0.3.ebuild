# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit font

DESCRIPTION="Cica font family"
HOMEPAGE="https://github.com/miiton/Cica"
SRC_URI="emoji? ( https://github.com/miiton/Cica/releases/download/v${PV}/Cica_v${PV}.zip -> ${P}.zip )
	!emoji? ( https://github.com/miiton/Cica/releases/download/v${PV}/Cica_v${PV}_without_emoji.zip -> ${P}-without-emoji.zip )"
S="${WORKDIR}"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE="+emoji"
RESTRICT="binchecks mirror strip"

BDEPEND="app-arch/unzip"

FONT_SUFFIX="ttf"
